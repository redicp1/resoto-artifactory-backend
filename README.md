# ReSoTo Artifactory

The repository contains artifact management system that stores generated code and its metadata under generation versions.

## Software Requirement

In order to run the ReSoTo project in development mode on Linux, macOS or Windows, the following
must be installed.

- [Docker][]
- [Docker Compose][]
- [Java SE 21][]
- [Maven]

## Getting started

Clone and setup the project.

> Note: your CLI should have elevated privileges when setting up and starting the app

```sh
git clone git@projects.fhict.nl:ixd/resoto/resoto-webform.git
mvn install
```

The project can be served using the following command.

```sh
mvn spring-boot:run
```

## Test and Deploy

The project can be tested using the following command.

```sh
mvn test
```

The project can be deployed to dockerhub.

```sh
docker login
docker build -t image_name .
docker tag image_name dockerhub_username/repository_name:tag
docker push dockerhub_username/repository_name:tag
```

## API Endpoints

Get all repositories:

```sh
/GET http://localhost:3000/azure-blob-storage/repositories
```

Get all versions:

```sh
/GET http://localhost:3000/azure-blob-storage/versions/{repositoryId}
```

Get all artifacts:

```sh
/GET http://localhost:3000/azure-blob-storage/artifacts/{repositoryId}/{version}
```

Start upload session:

```sh
/POST http://localhost:3000/artifacts/session/start
```

This request is going to generate a `session token`.

Upload artifact:

```sh
/POST http://localhost:3000/artifacts/session/upload
```

End upload session:

```sh
/POST http://localhost:3000/artifacts/session/end
```

# K8S & Azure Deployment

The folder of K8s contains deployment files and service files configurations that were used in the Azure Cloud Kubernetes Cluster.

pre-requirements:

1. Download kubectl for cluster communication.

To access the kubernetes cluster visit:
https://portal.azure.com/#@fhictsky.onmicrosoft.com/resource/subscriptions/4cf6ab57-baff-488b-be24-e8a961364804/resourceGroups/RGR-ReSoTo/providers/Microsoft.ContainerService/managedClusters/ReSoToCluster/overview

Be avare that an access for Azure environment and Cluster needs to be granted by the stakeholders, for us Jeffrey Cornelissen

To communicate wiht the cluster, the following steps need to be taken:

1. Download azure CLI from: https://learn.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest
2. Login to with azure CLI: in the command line use "az login".
3. Login with the corresponding account into Azure environment and access the cluster. Execute the following commands in the "Connect" section to connect to the cluster via the console:
   ![Alt text](image.png)
4. Test the connection: (kubectl required), following command "kubectl get pods" should give an overview of running pods in default namespace in the Azure Cluster.

# CI/CD

The structure of the pipeline can be found in the "gitlab-ci.yaml" and contains the following steps:

1. build stage
2. test stage
3. SonarCloud code analysis
4. Deployment of the images to DockerHub Registry

[docker]: https://docker.com
[docker compose]: https://docs.docker.com/compose
[java se 21]: https://www.oracle.com/java/technologies/javase/jdk21-archive-downloads.html
[maven]: https://maven.apache.org/download.cgi
