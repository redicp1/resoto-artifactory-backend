package com.resoto.artifactory.models;

import java.util.Map;

public record StartUploadSessionRequestDTO(long userID, long projectID, String projectName, String buildID,
                Map<String, String> additionalMetadata) {
}
