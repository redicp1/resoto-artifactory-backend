package com.resoto.artifactory.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true) // Add this annotation
public class UserAndRepositoryDetailsDTO {
    long user_id;
    long project_id;
    String project_name;

    public long getProjectID() {
        return project_id;
    }
}
