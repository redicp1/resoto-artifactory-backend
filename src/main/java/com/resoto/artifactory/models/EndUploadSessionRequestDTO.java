package com.resoto.artifactory.models;

public record EndUploadSessionRequestDTO(String sessionId) {
}
