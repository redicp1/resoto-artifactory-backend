package com.resoto.artifactory.models;

import java.util.Map;

public record ArtifactWithMetadataDTO(String artifactName, Map<String, String> artifactMetadata) {
}
