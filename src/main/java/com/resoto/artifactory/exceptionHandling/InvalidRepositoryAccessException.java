package com.resoto.artifactory.exceptionHandling;

public class InvalidRepositoryAccessException extends Exception {

    public InvalidRepositoryAccessException() {
        super("User does not have access to this project.");

    }

}
