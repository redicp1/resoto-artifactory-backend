package com.resoto.artifactory.exceptionHandling;

import com.azure.core.exception.AzureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Map;

@Service
public class HandleUploadException {

    public ResponseEntity<Map<String, String>> httpResponseMessage(Exception exception) {

        if (exception instanceof AzureException) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(Collections.singletonMap("error", exception.getMessage()));
        } else if (exception instanceof IllegalArgumentException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(Collections.singletonMap("error", exception.getMessage()));
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(Collections.singletonMap("error", exception.getMessage()));
        }
    }
}
