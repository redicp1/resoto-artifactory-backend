package com.resoto.artifactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArtifactoryApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArtifactoryApplication.class);

    public static void main(String[] args) {
        LOGGER.info("Starting Artifactory Application");
        SpringApplication.run(ArtifactoryApplication.class, args);
    }
}
