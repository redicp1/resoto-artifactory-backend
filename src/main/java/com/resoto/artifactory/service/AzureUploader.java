package com.resoto.artifactory.service;

import com.azure.core.exception.AzureException;
import com.azure.storage.blob.BlobClient;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

@Service
public class AzureUploader {

    public void uploadArtifact(BlobClient blobClient, MultipartFile file, Map<String, String> metadata)
            throws Exception {

        try {

            Path tempFile = Files.createTempFile("upload-", file.getOriginalFilename());
            file.transferTo(tempFile);

            System.out.println(blobClient.getBlobName());

            blobClient.upload(Files.newInputStream(tempFile), file.getSize(), true);

            blobClient.setMetadata(metadata);
            // Clean up the temporary file
            Files.delete(tempFile);
        } catch (AzureException e) {
            throw new AzureException("why: " + e.getMessage());
        } catch (Exception e) {
            System.err.println("Error during file upload: " + e.getMessage());
            throw new Exception("help:" + e.getMessage());
        }

    }
}
