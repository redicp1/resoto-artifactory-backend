package com.resoto.artifactory.service;

import com.azure.storage.blob.BlobClient;
import com.azure.storage.blob.BlobContainerClient;
import com.azure.storage.blob.BlobServiceClient;
import com.azure.storage.blob.models.BlobItem;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Service
@AllArgsConstructor
public class BlobStorageUploader {

    @Autowired
    BlobServiceClient getBlobServiceClient;

    public void uploadFile(String projectName, MultipartFile file) {

        // Format the container name to be valid
        String containerName = projectName.toLowerCase().replace(" ", "-");

        // Get or create the container for the project
        BlobContainerClient containerClient = getBlobServiceClient.getBlobContainerClient(containerName);
        if (!containerClient.exists()) {
            containerClient.create();
            System.out.println("Created container for project: " + projectName);
        }

        // Determine the next version folder and construct the blob name
        String version = getNextVersion(containerClient);
        String blobName = version + "/" + file.getOriginalFilename();

        // Upload the file
        try {
            BlobClient blobClient = containerClient.getBlobClient(blobName);
            Path tempFile = Files.createTempFile("upload-", file.getOriginalFilename());
            file.transferTo(tempFile);
            blobClient.upload(Files.newInputStream(tempFile), file.getSize(), true);
            System.out.println("Uploaded file to: " + blobName);

            // Clean up the temporary file
            Files.delete(tempFile);
        } catch (IOException e) {
            System.err.println("Error during file upload: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private String getNextVersion(BlobContainerClient containerClient) {
        String versionPrefix = "v";
        int maxVersion = 0;

        // Find the highest existing version number
        for (BlobItem blobItem : containerClient.listBlobs()) {
            String blobPath = blobItem.getName();
            if (blobPath.contains("/")) {
                String versionPart = blobPath.substring(0, blobPath.indexOf('/'));
                if (versionPart.startsWith(versionPrefix)) {
                    try {
                        int currentVersion = Integer.parseInt(versionPart.substring(1));
                        maxVersion = Math.max(maxVersion, currentVersion);
                    } catch (NumberFormatException e) {
                        // Ignore invalid version parts
                    }
                }
            }
        }

        // Return the next version
        return versionPrefix + (maxVersion + 1);
    }

}
