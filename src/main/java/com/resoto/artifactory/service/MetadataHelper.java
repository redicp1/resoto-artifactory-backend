package com.resoto.artifactory.service;

import com.resoto.artifactory.models.StartUploadSessionRequestDTO;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Service
public class MetadataHelper {

    // Add any common metadata for each generated artifact + space to add more
    // custom metadata here
    public Map<String, String> getCommonMetadata(StartUploadSessionRequestDTO requestDTO) {

        Map<String, String> commonMetadata = new HashMap<>();
        commonMetadata.put("build_id", requestDTO.buildID());

        LocalDateTime generationTime = LocalDateTime.now();
        commonMetadata.put("generation_date", generationTime.toString());

        commonMetadata.putAll(requestDTO.additionalMetadata());

        return commonMetadata;
    }

}
