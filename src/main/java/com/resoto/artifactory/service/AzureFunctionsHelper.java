package com.resoto.artifactory.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.resoto.artifactory.models.UserAndRepositoryDetailsDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.type.TypeReference;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Service
public class AzureFunctionsHelper {

    @Value("${azure.functions.get.repositories.key}")
    private String getRepositoriesKey;

    @Value("${azure.functions.get.base.url}")
    private String repositoriesBaseURL;

    @Value("${azure.functions.add.repositories.key}")
    private String createRepositoryKey;

    private final ObjectMapper objectMapper = new ObjectMapper();

    public List<UserAndRepositoryDetailsDTO> getUserRepositories(long userId) {
        String combinedURL = String.format("%s/%d?code=%s", repositoriesBaseURL, userId, getRepositoriesKey);
        System.out.println("Calling URL: " + combinedURL);

        try {
            URL url = new URL(combinedURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "application/json");

            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder response = new StringBuilder();
                String inputLine;

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                System.out.println("Success: " + response);

                // Deserialize JSON response into List<UserAndRepositoryDetailsDTO>
                System.out.println(objectMapper.readValue(response.toString(),
                        new TypeReference<List<UserAndRepositoryDetailsDTO>>() {
                        }));
                return objectMapper.readValue(response.toString(),
                        new TypeReference<List<UserAndRepositoryDetailsDTO>>() {
                        });
            } else {
                System.out.println("Failed to fetch data with status: " + responseCode);
                return new ArrayList<>();
            }
        } catch (Exception e) {
            System.out.println("Error during API call: " + e.getMessage());
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public int saveUserRepository(long userId, long projectId, String projectName) {
        String combinedURL = String.format("%s?code=%s", repositoriesBaseURL, getRepositoriesKey);
        System.out.println("Calling URL: " + combinedURL);

        HttpURLConnection connection = null;
        try {
            URL url = new URL(combinedURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(true);

            String jsonInputString = String.format("{\"user_id\": %d, \"project_id\": %d, \"project_name\": \"%s\"}",
                    userId, projectId, projectName);
            try (OutputStream os = connection.getOutputStream()) {
                byte[] input = jsonInputString.getBytes(StandardCharsets.UTF_8);
                os.write(input, 0, input.length);
            }

            int responseCode = connection.getResponseCode();
            System.out.println("Response Code: " + responseCode);

            return responseCode;
        } catch (Exception e) {
            System.out.println("Error during API call: " + e.getMessage());
            e.printStackTrace();
            return HttpURLConnection.HTTP_INTERNAL_ERROR; // Return 500 in case of exceptions
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    public boolean userHasAccess(long userID, long repositoryID) {

        List<UserAndRepositoryDetailsDTO> repositories = getUserRepositories(userID);

        for (UserAndRepositoryDetailsDTO repo : repositories) {
            if (repo.getProjectID() == repositoryID) {
                return true;
            }
        }
        return false;
    }
}
