package com.resoto.artifactory.service;

import com.azure.core.exception.AzureException;
import com.azure.storage.blob.BlobClient;
import com.azure.storage.blob.BlobContainerClient;
import com.resoto.artifactory.models.StartUploadSessionRequestDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.UUID;

@Service
public class UploadSessionService {

    private final Map<String, Map<String, String>> sessionStore = new HashMap<>();

    @Autowired
    AzureUploader azureUploader;

    @Autowired
    AzureBlobHelper azureBlobHelper;

    @Autowired
    MetadataHelper metadataHelper;

    @Autowired
    AzureFunctionsHelper azureFunctionsHelper;

    private String genVersion;
    private BlobContainerClient containerClient;

    public String startSession(StartUploadSessionRequestDTO request) {

        String sessionId = UUID.randomUUID().toString();
        sessionStore.put(sessionId, metadataHelper.getCommonMetadata(request));

        String containerID = request.projectID() + "";

        try {
            // 1. create and get the Blob Container based on the project name
            containerClient = azureBlobHelper.getProjectContainerClient(containerID);

            if (!azureFunctionsHelper.userHasAccess(request.userID(), request.projectID())) {
                azureFunctionsHelper.saveUserRepository(request.userID(), request.projectID(), request.projectName());
            }
            // 2. create new Generation number for this session (gen1/,gen2/)
            genVersion = azureBlobHelper.createNewGenVersionFolder(containerClient);

            return sessionId;

        } catch (AzureException e) {
            throw new AzureException(e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(e.getMessage());
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public Map<String, String> getSessionMetadata(String sessionId) throws InputMismatchException {
        if (sessionId == null) {
            throw new InputMismatchException("Uploading session not running");
        }
        return sessionStore.get(sessionId);
    }

    public void uploadArtifact(String sessionId, MultipartFile artifact, Map<String, String> additionalMetadata) {

        if (getSessionMetadata(sessionId) == null) {
            throw new InputMismatchException("Uploading session not running");
        }

        try {

            // 1. Step to combine Metadata
            Map<String, String> commonMetadata = getSessionMetadata(sessionId);
            Map<String, String> combinedMetadata = new HashMap<>(commonMetadata);
            combinedMetadata.putAll(additionalMetadata);
            combinedMetadata.remove("sessionId");

            // 2. create path for storing with generation version and artifact name
            String blobClientPath = azureBlobHelper.createBlobClientPath(genVersion, artifact);
            BlobClient blobClient = containerClient.getBlobClient(blobClientPath);

            azureUploader.uploadArtifact(blobClient, artifact, combinedMetadata);

        } catch (AzureException e) {
            throw new AzureException(e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(e.getMessage());
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public Map<String, String> endSession(String sessionId) {
        Map<String, String> response = getSessionMetadata(sessionId);

        if (getSessionMetadata(sessionId) == null) {
            throw new InputMismatchException("Uploading has been stopped already");
        }
        sessionStore.remove(sessionId);

        return response;

    }

}
