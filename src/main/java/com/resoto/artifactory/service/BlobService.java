package com.resoto.artifactory.service;

import com.azure.core.exception.AzureException;
import com.azure.core.http.policy.ExponentialBackoffOptions;
import com.azure.core.http.policy.RetryOptions;
import com.azure.core.http.policy.RetryPolicy;
import com.azure.storage.blob.BlobClient;
import com.azure.storage.blob.BlobContainerClient;
import com.azure.storage.blob.BlobServiceClientBuilder;
import com.azure.storage.blob.models.BlobItem;
import com.azure.storage.blob.models.BlobProperties;
import com.resoto.artifactory.models.ArtifactWithMetadataDTO;
import com.resoto.artifactory.models.UserAndRepositoryDetailsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.*;

@Service
public class BlobService {

    @Autowired
    AzureBlobHelper azureBlobHelper;

    @Value("${azure.storage.connection.string}")
    private String connectionString;

    @Autowired
    AzureFunctionsHelper azureFunctionsHelper;

    private RetryPolicy retryPolicy() {
        ExponentialBackoffOptions backoff = new ExponentialBackoffOptions();

        backoff.setMaxRetries(3);
        backoff.setBaseDelay(Duration.ofSeconds(10));
        backoff.setMaxDelay(Duration.ofSeconds(20));

        return new RetryPolicy(new RetryOptions(backoff));
    }

    public List<UserAndRepositoryDetailsDTO> getAllRepositories(long userId) {

        return azureFunctionsHelper.getUserRepositories(userId);
    }

    public List<ArtifactWithMetadataDTO> getAllArtifacts(String userID, String repositoryId, String version) {

        List<ArtifactWithMetadataDTO> artifactsWithMetadata = new ArrayList<>();

        try {

            long userId = Long.parseLong(userID);
            long repoId = Long.parseLong(repositoryId);

            if (!azureFunctionsHelper.userHasAccess(userId, repoId)) {
                throw new AzureException("Repository not accessible");

            }

            // Build the blob service client with the connection string
            BlobServiceClientBuilder blobServiceClientBuilder = new BlobServiceClientBuilder()
                    .connectionString(connectionString);

            // Get the blob container client for the repository folder
            BlobContainerClient blobContainerClient = blobServiceClientBuilder.buildClient()
                    .getBlobContainerClient(repositoryId);

            // Iterate over the list of blobs in the repository folder
            for (BlobItem blobItem : blobContainerClient.listBlobs()) {
                String blobName = blobItem.getName();

                // Trim and normalize the version and blobName
                String trimmedVersion = version.trim();
                String normalizedBlobName = blobName.trim();

                // Check if the blob name starts with the specified version
                if (normalizedBlobName.startsWith(trimmedVersion + "/")) {
                    // Check if the blobName is longer than the version string
                    if (normalizedBlobName.length() > trimmedVersion.length()) {
                        // Remove the version part from the blob name
                        String artifactName = normalizedBlobName.substring(trimmedVersion.length() + 1);

                        BlobClient blobClient = blobContainerClient.getBlobClient(blobName);
                        BlobProperties properties = blobClient.getProperties();

                        Map<String, String> artifactMetadata = properties.getMetadata();
                        ArtifactWithMetadataDTO artifactWithMetadataDTO = new ArtifactWithMetadataDTO(artifactName,
                                artifactMetadata);
                        artifactsWithMetadata.add(artifactWithMetadataDTO);

                    }
                }
            }
            // Return the list of artifact names after the specified version
            return artifactsWithMetadata;

        } catch (AzureException e) {
            throw new AzureException(e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(e.getMessage());
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

    }

    public List<String> getAllVersions(String userID, String repositoryId) {
        List<String> versionNames = new ArrayList<>();

        // if (!azureBlobHelper.userOwnsProject(userID,reposiotryName)){
        // throw("user does not have access to the repository") }

        try {
            // Build the blob service client with the connection string
            BlobServiceClientBuilder blobServiceClientBuilder = new BlobServiceClientBuilder()
                    .connectionString(connectionString)
                    .addPolicy(retryPolicy());

            // Get the blob container client for the repository folder
            BlobContainerClient blobContainerClient = blobServiceClientBuilder.buildClient()
                    .getBlobContainerClient(repositoryId);

            // Iterate over the list of blobs in the repository folder
            // Each blob represents a version (folder)
            for (BlobItem blobItem : blobContainerClient.listBlobs()) {
                String blobName = blobItem.getName();
                var newName = blobName.split("/")[0];
                if (!versionNames.contains(newName)) {
                    versionNames.add(newName);
                }
            }
            // Return the list of version names
            return versionNames;
        } catch (AzureException e) {
            throw new AzureException(e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(e.getMessage());
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
