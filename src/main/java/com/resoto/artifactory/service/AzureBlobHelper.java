package com.resoto.artifactory.service;

import com.azure.storage.blob.BlobContainerClient;
import com.azure.storage.blob.BlobServiceClient;
import com.azure.storage.blob.models.BlobItem;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@AllArgsConstructor
public class AzureBlobHelper {

    @Autowired
    BlobServiceClient getBlobServiceClient;

    @Autowired
    AzureFunctionsHelper azureFunctionsHelper;

    // User_ID
    // store projectName for specified user_id if not created
    // calling the azure functions

    public BlobContainerClient getProjectContainerClient(String containerName) throws Exception {

        if (containerName == null || containerName.trim().isEmpty()) {
            throw new IllegalArgumentException("Project name must not be null or empty");
        }

        try {
            // Get or create the container for the project
            BlobContainerClient containerClient = getBlobServiceClient.getBlobContainerClient(containerName);
            if (!containerClient.exists()) {
                containerClient.create();
                System.out.println("Created container for project: " + containerName);
                return containerClient;
            }
            return containerClient;

        } catch (Exception error) {
            throw new Exception("Failed to create container" + error.getMessage());
        }
    }

    public String createBlobClientPath(String genVersionFolderName, MultipartFile artifact) {

        return genVersionFolderName + artifact.getOriginalFilename();

    }

    public String createNewGenVersionFolder(BlobContainerClient containerClient) {

        return getNextVersion(containerClient) + "/";

    }

    private String getNextVersion(BlobContainerClient containerClient) {
        String versionPrefix = "v";
        int maxVersion = 0;

        // Find the highest existing version number
        for (BlobItem blobItem : containerClient.listBlobs()) {
            String blobPath = blobItem.getName();
            if (blobPath.contains("/")) {
                String versionPart = blobPath.substring(0, blobPath.indexOf('/'));
                if (versionPart.startsWith(versionPrefix)) {
                    try {
                        int currentVersion = Integer.parseInt(versionPart.substring(1));
                        maxVersion = Math.max(maxVersion, currentVersion);
                    } catch (NumberFormatException e) {
                        // Ignore invalid version parts
                    }
                }
            }
        }

        // Return the next version
        return versionPrefix + (maxVersion + 1);
    }

}
