package com.resoto.artifactory.controllers;

import com.resoto.artifactory.models.EndUploadSessionRequestDTO;
import com.resoto.artifactory.models.StartUploadSessionRequestDTO;
import com.resoto.artifactory.exceptionHandling.HandleUploadException;
import com.resoto.artifactory.service.UploadSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/artifacts/session", produces = "application/json")
public class SessionUploadController {

    @Autowired
    UploadSessionService uploadSessionService;

    @Autowired
    HandleUploadException handleUploadException;

    @PostMapping("/start")
    public ResponseEntity<Map<String, String>> startUploadSession(@RequestBody StartUploadSessionRequestDTO request) {

        try {
            String sessionId = uploadSessionService.startSession(request);
            Map<String, String> response = new HashMap<>();
            response.put("session_id", sessionId);
            return ResponseEntity.ok(response);
        } catch (Exception exception) {
            return handleUploadException.httpResponseMessage(exception);
        }
    }

    @PostMapping("/upload")
    public ResponseEntity<Map<String, String>> uploadArtifact(
            @RequestParam("sessionId") String sessionId,
            @RequestParam("artifact") MultipartFile artifact,
            @RequestParam Map<String, String> additionalMetadata) {

        try {
            uploadSessionService.uploadArtifact(sessionId, artifact, additionalMetadata);
            Map<String, String> response = new HashMap<>();
            response.put("status", "success");
            response.put("artifact_name", artifact.getOriginalFilename());
            return ResponseEntity.ok(response);
        } catch (Exception exception) {
            return handleUploadException.httpResponseMessage(exception);
        }
    }

    @PostMapping("/end")
    public ResponseEntity<Map<String, String>> endUploadSession(@RequestBody EndUploadSessionRequestDTO request) {

        try {

            Map<String, String> response = uploadSessionService.endSession(request.sessionId());
            response.put("status", "session_closed");

            return ResponseEntity.ok(response);
        } catch (Exception exception) {
            return handleUploadException.httpResponseMessage(exception);
        }

    }

}
