package com.resoto.artifactory.controllers;

import com.azure.core.exception.AzureException;
import com.resoto.artifactory.models.ArtifactWithMetadataDTO;
import com.resoto.artifactory.models.UserAndRepositoryDetailsDTO;
import com.resoto.artifactory.service.BlobService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(value = "/azure-blob-storage", produces = "application/json")
public class AzureBlobStorageController {

    private final BlobService blobService;

    @Autowired
    public AzureBlobStorageController(BlobService blobStorageService) {
        this.blobService = blobStorageService;
    }

    @PreAuthorize("hasAuthority('SCOPE_TEST')")
    @GetMapping("/repositories")
    public ResponseEntity<?> getAllRepositories(@RequestParam long userID) {
        try {
            List<UserAndRepositoryDetailsDTO> repositories = blobService.getAllRepositories(userID);
            return ResponseEntity.ok().body(repositories);
        } catch (AzureException error) {
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE)
                    .body(Collections.singletonList(error.getMessage()));
        } catch (IllegalArgumentException error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(Collections.singletonList(error.getMessage()));
        } catch (Exception error) {

            System.out.println(error.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(Collections.singletonList(error.getMessage()));
        }
    }

    // User_ID
    @GetMapping("/artifacts/{repositoryName}/{version}")
    public ResponseEntity<?> getAllArtifacts(@PathVariable String repositoryName,
            @PathVariable String version, @RequestParam String userID) {
        try {
            List<ArtifactWithMetadataDTO> artifacts = blobService.getAllArtifacts(userID, repositoryName, version);
            return ResponseEntity.ok().body(artifacts);
        } catch (AzureException error) {
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE)
                    .body(Collections.singletonList(error.getMessage()));
        } catch (IllegalArgumentException error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(Collections.singletonList(error.getMessage()));
        } catch (Exception error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(Collections.singletonList(error.getMessage()));
        }
    }

    @GetMapping("/versions/{repositoryName}")
    public ResponseEntity<List<String>> getAllVersions(@PathVariable String repositoryName,
            @RequestParam String userID) {
        try {
            List<String> versions = blobService.getAllVersions(userID, repositoryName);
            return ResponseEntity.ok().body(versions);
        } catch (AzureException error) {
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE)
                    .body(Collections.singletonList(error.getMessage()));
        } catch (IllegalArgumentException error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(Collections.singletonList(error.getMessage()));
        } catch (Exception error) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(Collections.singletonList(error.getMessage()));
        }
    }

}
