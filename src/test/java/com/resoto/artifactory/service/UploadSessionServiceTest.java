package com.resoto.artifactory.service;

import com.azure.storage.blob.BlobContainerClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.InputMismatchException;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class UploadSessionServiceTest {

    @Mock
    private AzureUploader azureUploader;

    @Mock
    private AzureBlobHelper azureBlobHelper;

    @Mock
    private MetadataHelper metadataHelper;

    @Mock
    private BlobContainerClient containerClient;

    @InjectMocks
    private UploadSessionService uploadSessionService;

    @Mock
    private AzureFunctionsHelper azureFunctionsHelper;

    @BeforeEach
    void setUp() {

    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testGetSessionMetadataWhenSessionDoesNotExist() {
        assertThrows(InputMismatchException.class, () -> uploadSessionService.getSessionMetadata(null));
    }

    @Test
    public void testEndSessionAlreadyStopped() {
        String sessionId = "session123";
        assertThrows(InputMismatchException.class, () -> uploadSessionService.endSession(sessionId)); // This should
                                                                                                      // throw an
                                                                                                      // exception
    }

}
